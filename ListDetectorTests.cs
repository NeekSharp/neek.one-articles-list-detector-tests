﻿using System;
using NUnit.Framework;
using System.Collections.Generic;
using CommonLibrary;

namespace ArticlesListDetector.Test
{
    [TestFixture]
    public class ListDetectorTests
    {
        [TestCase("<div><div class='fb-comments'></div></div>", true)]
        [TestCase("<div><div class='fb-comm'></div></div>", false)]
        public void HasScriptsForComment_HtmlWithSpecificElements_TrueResult(string html, bool result)
        {
            // Arrange
            WebPage page = GetWegPage(html);

            // Act
            ListDetector dwpt = new ListDetector(page);

            // Assert
            Assert.IsTrue(dwpt.HasScriptsForComment() == result);
        }

        
        [TestCase("<div> <p></p><p></p> <p></p> </div><div> <p></p> <p></p> <p></p> <p></p> </div>")]
        public void MaxParagraphsInOneElement_HtmlWithParagraphs_RightParagraphsCount(string body)
        {
            // Arrange
            WebPage page = GetWegPage(body);
            ListDetector dwpt = new ListDetector(page);

            // Act
            int count = dwpt.MaxParagraphsInOneElement();

            // Assert
            Assert.IsTrue(count == 4);
        }

        [Test]
        public void DashesIsUniCount_LinkWithDashes_RightDashesCount()
        {
            // Arrange
            WebPage page = GetWegPage("", "http://a.ru/asd/a-a-a-a-a---a");
            ListDetector dwpt = new ListDetector(page);

            // Act
            int count = dwpt.DashesIsUriCount();

            // Assert
            Assert.IsTrue(count == 7);
        }

        [Test]
        public void LinkToAuthorPages_HtmlWithAuthorLink_LinksCount()
        {
            // Arrange
            WebPage page = GetWegPage();

            page.InternalLinks = new List<Link>()
                {
                    new Link("http://habrahabr.ru/editor/joe"), // +
                    new Link("http://habrahabr.ru/user/joe"),   // +
                    new Link("http://habrahabr.ru/bla/joe"),    // -
                };

            ListDetector dwpt = new ListDetector(page);

            // Act
            int count = dwpt.LinkToAuthorPagesCount();

            // Assert
            Assert.IsTrue(count == 2);
        }

        [TestCase(@"<div class='fb-comm' role='article'></div>
                    <div class='fb-comm' role='post'></div>
                    <div class='fb-comm' role='article'></div>"
                    ,true)]

        [TestCase(@"<div class='fb-comm1'></div>
                    <div class='fb-comm2'></div>
                    <div class='fb-comm3'></div>"
                    ,false)]
        public void HasElementWithChildrenHavingPositiveAttributesForList_HtmlWithAttributes_TrueResult(string body,bool result)
        {
            WebPage page = GetWegPage(body);

            ListDetector dwpt = new ListDetector(page);

            Assert.IsTrue(dwpt.HasElementWithChildrenHavingPositiveAttributesForList() == result);
        }

        [Test]
        public void ReadMoreLinksCount_HtmlWithReadMoreLink_LinksCount()
        {
            // Arrange
            WebPage page = GetWegPage();
            page.InternalLinks = new List<Link>()
                {
                    new Link("http://habrahabr.ru/editor/joe","Read more"),
                    new Link("http://habrahabr.ru/user/joe","Читать далее"),
                    new Link("http://habrahabr.ru/bla/joe","Читать дальше"),
                    new Link("http://habrahabr.ru/bla/joe","Читать целиком"),
                    new Link("http://habrahabr.ru/bla/joe","Continue reading"),
                };

            ListDetector dwpt = new ListDetector(page);

            // Act
            int count = dwpt.ReadMoreLinksCount();

            // Assert
            Assert.IsTrue(count == 5);
        }

        [Test]
        public void LastSegmentPageUriIsDigit_UriWithAndWithoutDigits_CorrectResult()
        {
            List<Uri> uri_true = new List<Uri>()
            {
                new Uri("http://habrahabr.ru/interesting/page/2"),      // 2
                new Uri("http://habrahabr.ru/interesting/page/2/"),     // 3
                new Uri("http://habrahabr.ru/interesting/2014-12-13"),  // 4
                new Uri("http://habrahabr.ru/interesting/1-12-12"),     // 5
                new Uri("http://habrahabr.ru/interesting/2014-12-13/"), // 6
                new Uri("http://habrahabr.ru/interesting/20141213/"),   // 7
                
            };

            List<Uri> uri_false = new List<Uri>()
            {
                new Uri("http://habrahabr.ru/interesting/20141213a"),   // 8
                new Uri("http://habrahabr.ru/interesting/page2/"),      // 0
                new Uri("http://habrahabr.ru/interesting/lalala/"),     // 1
            };

            WebPage page = GetWegPage();

            var listDetector = new ListDetector(page);
            
            foreach (Uri uri in uri_true)
            {
                Assert.IsTrue(listDetector.LastSegmentUriIsDigitOrDate(uri));
            }

            foreach (Uri uri in uri_false)
            {
                Assert.IsFalse(listDetector.LastSegmentUriIsDigitOrDate(uri));
            }
        }

        [Test]
        public void ChildPages_HtmlWithLinksStartsWithBase_LinksStartsWithBaseCount()
        {
            WebPage page = GetWegPage("","http://habrahabr.ru/interesting/");
            page.InternalLinks = new List<Link>()
            {
                new Link("http://habrahabr.ru/interesting/page2/"),         // +
                new Link("http://habrahabr.ru/interesting/lalala/"),
                new Link("http://habrahabr.ru/interesting/page/2"),         // +
                new Link("http://habrahabr.ru/interesting?asdasd=dasdad"),
                new Link("http://habrahabr.ru/interesting?page=2")          // +
            };

            ListDetector dwpt = new ListDetector(page);
            int result = dwpt.ChildPagesCount();
            Assert.IsTrue(result == 3);
        }

        private WebPage GetWegPage(string htmlbody = "",string url = "http://ya.ru")
        {
            WebPage page = new WebPage(new Link("http://ya.ru"));
            page.Html = String.Format(@"<!doctype html> <html> <body>{0}</body> </html>",htmlbody);
            return page;
        }

    }
}
