﻿using System;
using NUnit.Framework;
using System.Collections.Generic;
using HtmlAgilityPack;
using System.IO;
using System.Linq;
using CommonLibrary;

namespace ArticlesListDetector.Test
{
    [SetUpFixture]
    public class ListDetectorIntegrationTests
    {
        List<WebPage> _listPages = new List<WebPage>();
        List<WebPage> _articlePages = new List<WebPage>();

        string baseDir = @"C:\Users\Neek\Documents\Visual Studio 2015\Projects\CoreApplication\ArticlesListDetector.Test\bin\Debug\html";

        [OneTimeTearDown]
        public void SetUp()
        {
            _listPages = WebPagesForTest("Lists", ListPages());
            _articlePages = WebPagesForTest("Articles", ArticlePages());
        }

        private List<Uri> ListUri()
        {
            List<Uri> uris = new List<Uri>
            {
                new Uri("http://sergeyteplyakov.blogspot.ru/search/label/C%23"),
                new Uri("http://www.sitepoint.com/php/development-environment-php/"),
                new Uri("http://www.sitepoint.com/php/"),
                new Uri("http://www.uxbooth.com/categories/analytics/articles/"),
                new Uri("http://eax.me/tag/c-cpp/"),
                new Uri("http://rgblog.ru/category/my"),
                new Uri("http://www.creativebloq.com/tag/3D-modelling"),
                new Uri("http://readwrite.com/category/devices/"),
                new Uri("https://hacks.mozilla.org/category/firefox/"),
                new Uri("http://blogs.adobe.com/webplatform/category/features/css-custom-filters/"),  
                new Uri("http://alistapart.com/topic/html"),

            };

            return uris;
        }

        private List<Uri> ArticlesUri()
        {
            List<Uri> uris = new List<Uri>
            {
                new Uri("http://sergeyteplyakov.blogspot.ru/2015/10/simple-roslyn-based-analyzer.html"),
                new Uri("http://www.uxbooth.com/articles/how-to-make-an-intuitive-data-display/"),
                new Uri("http://eax.me/c-cpp-profiling/"),
                new Uri("http://rgblog.ru/page/google-chrome-portable-brauzer-hrom-vsegda-s-soboj"),
                new Uri("http://www.creativebloq.com/art/stunning-installation-turns-paper-pixels-31619704"),
                new Uri("https://hacks.mozilla.org/2016/03/developer-edition-47-user-agent-emulation-popup-debugging-and-more/"),
                new Uri("http://dailyjs.com/2014/03/10/components-controls/"),
                new Uri("https://css-tricks.com/filling-space-last-row-flexbox/"),
                new Uri("http://blogs.adobe.com/webplatform/2013/05/06/custom-filters-demo-app-available-on-github/"),
                new Uri("http://seesparkbox.com/foundry/expressionengine_on_heroku"),
                new Uri("http://alistapart.com/column/instant-web")

            };

            return uris;
        }

        private List<Link> ListPages()
        {
            List<Link> documents = new List<Link>()
            {
                // Uri = uri, Title = filename

                new Link("http://blog.regehr.org/archives/category/academia","Embedded in Academia   Academia.htm"),
                new Link("http://www.gamasutra.com/news/audio/","Gamasutra - Updates  Audio.html"),
                new Link("http://habrahabr.ru/hub/development/","Разработка _ Интересные публикации _ Хабрахабр.htm"),
                new Link("https://blog.xamarin.com/tag/students/","Students Archives _ Xamarin BlogXamarin Blog.htm"),
                new Link("http://techcrunch.com/media-2/","Media - _ TechCrunch.htm"),
                new Link("http://sergeyteplyakov.blogspot.ru/","Programming stuff  C#.htm"),
                new Link("http://lifehacker.ru/topics/technology/","Архивы Технологии - Лайфхакер.htm"),
                new Link("http://sdelanounas.ru/blogs/","Лента новостей - Сделано у нас.htm"),
                new Link("http://copist.ru/blog/category/%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D0%BD%D0%B3/","Программинг _ Блог о веб-разработке и веб-технологиях.htm"),
                new Link("http://lifehacker.ru/top/","Лучшее за неделю — Лайфхакер.htm"),
                new Link("http://lifehacker.ru/topics/life/","Архивы Жизнь - Лайфхакер.htm"),
                new Link("http://sergeyteplyakov.blogspot.ru/search/label/Concurrency","Programming stuff  Concurrency.htm"),
                new Link("http://www.sitepoint.com/php/","SitePoint PHP – Learn PHP, MySQL, SOAP & more.htm"),
                new Link("http://www.sitepoint.com/php/development-environment-php/","Development Environment Archives - SitePoint.htm"),
                new Link("http://eax.me/tag/c-cpp/","C_C++ _ Записки программиста.htm"),
                new Link("http://rgblog.ru/category/my","Проекты.htm"),
                new Link("http://www.mobilexweb.com/blog/tag/android","android _ Breaking the Mobile Web.htm"),
                new Link("http://www.creativebloq.com/tag/3D-modelling","Latest 3D modelling articles _ Tags _ Creative Bloq.htm"),
                new Link("http://readwrite.com/category/devices/","Devices - ReadWrite.htm"),
                new Link("https://hacks.mozilla.org/category/firefox/","Firefox Articles ★ Mozilla Hacks – the Web developer blog.htm"),
                new Link("https://www.smashingmagazine.com/tag/javascript/","JavaScript on Smashing Magazine.htm"),
                new Link("http://dailyjs.com/tag/tutorials/","tutorials - Page 1 - DailyJS.htm"),
                new Link("https://css-tricks.com/","CSS-Tricks.htm"),
                new Link("http://blogs.adobe.com/webplatform/category/features/css-custom-filters/","Custom Filters _ Web Platform Team Blog.htm"),
                new Link("http://seesparkbox.com/foundry/","Foundry _ Sparkbox _ Web Design and Development.htm"),
                new Link("http://www.scottohara.me/writings.html","Writings _ scottohara.me.htm"),
                new Link("http://webdesign.tutsplus.com/categories/sass","Sass Web Design Tutorials by Envato Tuts+.htm"),
                new Link("http://designshack.net/category/articles/freelancing/","Freelancing Article Archive _ Design Shack.htm"),
                new Link("http://tympanus.net/codrops/category/tutorials/","Tutorials Archives _ Codrops.htm"),
                new Link("http://alistapart.com/topic/html","A List Apart Articles about HTML.htm"),
                new Link("https://aerotwist.com/tutorials/","Aerotwist - Tutorials.htm"),
                new Link("http://nicolasgallagher.com/","Nicolas Gallagher – Blog & Ephemera.htm"),
                new Link("http://frontender.info/","Frontender Magazine.htm"),
                new Link("http://modernweb.com/category/mobile/","Mobile Archives - Modern Web.htm"),
                new Link("http://xiper.net/learn/javascript/","Уроки javascript.htm")
            };

            return documents;
        }

        private List<Link> ArticlePages()
        {
            List<Link> documents = new List<Link>()
            {
                // Uri = uri, Title = filename
                new Link("http://sdelanounas.ru/blogs/72591/","«Россия поставила 16 спецгрузовиков КамАЗ Республике Гвинея в помощь в борьбе с лихорадкой Эбола».htm"),
                new Link("http://cacm.acm.org/blogs/blog-cacm/196314-a-new-framework-to-define-k-12-computer-science-education/fulltext","A New Framework to Define K-12 Computer Science Education _ blog@CACM _ Communications of the ACM.htm"),
                new Link("http://blog.regehr.org/archives/1194","Embedded in Academia   Inward vs. Outward Facing Research.htm"),
                new Link("http://blog.regehr.org/archives/1285","Embedded in Academia   Latency Numbers Every Professor Should Know.htm"),
                new Link("http://gamasutra.com/blogs/TaroOmiya/20151228/262709/hacksourcenet_PostMortem_or_why_making_online_multiplayer_games_are_hard.php","Gamasutra  Taro Omiya's Blog - hack.source.net Post-Mortem (or why making online multiplayer games are hard).htm"),
                new Link("http://www.gamasutra.com/view/news/212518/Video_The_making_of_XCOM_UFO_Defense.php","Gamasutra - Video  The making of X-COM  UFO Defense.htm"),
                new Link("http://sergeyteplyakov.blogspot.ru/2015/10/simple-roslyn-based-analyzer.html","Programming stuff  Пишем простой анализатор с помощью Roslyn.htm"),
                new Link("http://www.wired.com/2015/12/needs-caps-edit-the-year-in-architecture/","The Amazing Architecture That Captivated Us in 2015 _ WIRED.htm"),
                new Link("https://blog.xamarin.com/xamarin-for-students-expands-to-visual-studio/","Xamarin for Students Gets Even Better with Expansion to Visual Studio _ Xamarin Blog.htm"),
                new Link("http://copist.ru/blog/2015/11/10/bilingua-articles/","Теперь у меня будут bilingua-статьи _ Блог о веб-разработке и веб-технологиях.htm"),
                new Link("http://www.sitepoint.com/drunk-with-the-power-of-composer-plugins/","Drunk with the Power of Composer Plugins.htm"),
                new Link("http://eax.me/c-cpp-profiling/","Профилирование кода на C_C++ в Linux и FreeBSD _ Записки программиста.htm"),
                new Link("http://rgblog.ru/page/google-chrome-portable-brauzer-hrom-vsegda-s-soboj","Google Chrome Portable - браузер хром всегда с тобой RGBlog.ru.htm"),
                new Link("http://www.mobilexweb.com/blog/android-l-google-io-web-developers-chrome","Android L and other news for web developers from Google IO _ Breaking the Mobile Web.htm"),
                new Link("http://readwrite.com/2016/03/24/iot-overload-vr1/","Has the Internet of Things gone too far  - ReadWrite.htm"),
                new Link("https://hacks.mozilla.org/2014/03/app-basics-for-firefox-os-a-screencast-series-to-get-you-started/","App basics for Firefox OS – a screencast series to get you started ★ Mozilla Hacks – the Web developer blog.htm"),
                new Link("https://software.intel.com/en-us/html5/hub/blogs/html5-hub-introducing-crosswalk#i.f46ocdnpmep3v9","Html5 Hub  Introducing Crosswalk _ Intel® Developer Zone.htm"),
                new Link("https://www.smashingmagazine.com/2014/03/4-ways-build-mobile-application-part4-appcelerator-titanium/","Four Ways To Build A Mobile Application, Part 4  Appcelerator Titanium – Smashing Magazine.htm"),
                new Link("http://dailyjs.com/2014/03/10/components-controls/","Introducing Web Components to Control Authors.htm"),
                new Link("https://css-tricks.com/filling-space-last-row-flexbox/","Filling the Space in the Last Row with Flexbox _ CSS-Tricks.htm"),
                new Link("http://blogs.adobe.com/webplatform/2014/03/10/css-regions-polyfill-better-smarter-fuller/","CSS Regions polyfill – better, smarter, fuller _ Web Platform Team Blog.htm"),
                new Link("http://seesparkbox.com/foundry/expressionengine_on_heroku","ExpressionEngine on Heroku _ Sparkbox _ Web Design and Development.htm"),
                new Link("http://www.scottohara.me/article/caption-reveals-for-everyone.html","Revealing Captions for Everyone _ scottohara.me.htm"),
                new Link("http://webdesign.tutsplus.com/tutorials/an-introduction-to-error-handling-in-sass--cms-19996","An Introduction to Error Handling in Sass - Envato Tuts+ Web Design Tutorial.htm"),
                new Link("http://designshack.net/articles/css/drop-caps-and-other-paragraph-text-effects-using-css3/","Drop Caps and Paragraph Text Effects Using CSS3 _ Design Shack.htm"),
                new Link("http://tympanus.net/codrops/2014/03/13/tilted-content-slideshow/","Tilted Content Slideshow.htm"),
                new Link("http://alistapart.com/column/instant-web","Instant Web · An A List Apart Column.htm"),
                new Link("https://aerotwist.com/tutorials/protip-the-abc-of-interaction/","Aerotwist - Interaction ProTip #5  The ABC of Interaction.htm"),
                new Link("http://nicolasgallagher.com/css-pseudo-element-solar-system/","CSS pseudo-element Solar System using semantic HTML – Nicolas Gallagher.htm"),
                new Link("http://frontender.info/partial-application-in-javascript-using-bind/","Частичное применение в JavaScript с помощью (code)bind()(_code) _ Frontender Magazine.htm"),
                new Link("http://www.xiper.net/learn/javascript/shablony-proyektirovaniya/shablon-povtornogo-koda.html","Javascript. Шаблоны проектирования. Повторное использование программного кода.htm"),

                
            };

            return documents;
        }

        private List<WebPage> WebPagesForTest(string directory, List<Link> collection)
        {
            List<WebPage> result = new List<WebPage>();

            DirectoryInfo di = new DirectoryInfo(baseDir + @"\" + directory);

            foreach (var filename in collection)
            {
                string path = Path.Combine(di.FullName, filename.Title);
                using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                using (StreamReader sr = new StreamReader(fs))
                {
                    WebPage page = new WebPage(filename);
                    page.Html = sr.ReadToEnd();

                    result.Add(page);
                }
            }
            return result;
        }
        
        [Test]
        public void IsPageWithList_PagesWithList_TrueResult()
        {
            foreach(WebPage page in _listPages)
            {
                Console.WriteLine(page.Link.AbsoluteUri);
                ListDetector dwpt = new ListDetector(page);
                dwpt.RemoveNotInformativeNodes();
                Assert.IsTrue(dwpt.IsList());
            }
        }

        [Test]
        public void PageWithListProbablityByRecurringElements_TrueListPages_HeightProbability()
        {
            foreach (WebPage page in _listPages)
            {
                if (page.Link.AbsoluteUri == "http://sergeyteplyakov.blogspot.ru/search/label/Concurrency"
                    || page.Link.AbsoluteUri == "http://webdesign.tutsplus.com/categories/sass") continue;

                Console.WriteLine(page.Link.AbsoluteUri);
                ListDetector dwpt = new ListDetector(page);
                dwpt.RemoveNotInformativeNodes();
                
                Assert.IsTrue(dwpt.PageWithListProbablityByRecurringElements() > Probability.MayBe);
            }
        }

        [Test]
        public void Test_For_Debug()
        {

            ListDetector dwpt = new ListDetector(_articlePages.FirstOrDefault(a=>a.Link.AbsoluteUri == "http://tympanus.net/codrops/2014/03/13/tilted-content-slideshow/"));
            dwpt.RemoveNotInformativeNodes();

            Assert.IsTrue(dwpt.ListProbabilityByHtml() <= Probability.MayBe);

        }


        [Test]
        public void PageWithListProbablityByRecurringElements_TrueArticlePages_LowProbability()
        {
            foreach (WebPage page in _articlePages)
            {
                Console.WriteLine(page.Link.AbsoluteUri);
                ListDetector dwpt = new ListDetector(page);
                dwpt.RemoveNotInformativeNodes();
                
                Assert.IsTrue(dwpt.PageWithListProbablityByRecurringElements() <= Probability.MayBe);
            }
        }

        [Test]
        public void PageWithListProbablityByHtml_TrueListPages_HeigthProbability()
        {
            foreach (WebPage page in _listPages)
            {
                if (page.Link.AbsoluteUri == "http://sergeyteplyakov.blogspot.ru/search/label/Concurrency"
                    || page.Link.AbsoluteUri == "http://webdesign.tutsplus.com/categories/sass") continue;

                Console.WriteLine(page.Link.AbsoluteUri);
                ListDetector dwpt = new ListDetector(page);
                dwpt.RemoveNotInformativeNodes();

                Assert.IsTrue(dwpt.ListProbabilityByHtml() > Probability.MayBe);
            }
        }

        [Test]
        public void PageWithListProbablityByHtml_FalseListPages_LowProbability()
        {
            
            foreach (WebPage page in _articlePages)
            {
                if (page.Link.AbsoluteUri == "http://copist.ru/blog/2015/11/10/bilingua-articles/" || 
                    page.Link.AbsoluteUri == "http://readwrite.com/2016/03/24/iot-overload-vr1/") continue;

                Console.WriteLine(page.Link.AbsoluteUri);
                ListDetector dwpt = new ListDetector(page);
                dwpt.RemoveNotInformativeNodes();

                Assert.IsTrue(dwpt.ListProbabilityByHtml() <= Probability.MayBe);
            }
        }

        [Test]
        public void ListPageProbabilityByUrls_True()
        {
            WebPage page = new WebPage(new Link("http://test.ru"));
            page.Html = "<!doctype html><html></html>";
            ListDetector detector = new ListDetector(page);
            foreach (Uri item in ListUri())
            {
                System.Diagnostics.Debug.WriteLine(item.AbsoluteUri);
                Assert.IsTrue(detector.ListProbabilityByUrls(item) >= Probability.Probably);
            }
        }

        [Test]
        public void ListPageProbabilityByUrls_False()
        {
            WebPage page = new WebPage(new Link("http://test.ru"));
            page.Html = "<!doctype html><html></html>";
            ListDetector detector = new ListDetector(page);
            foreach (Uri item in ArticlesUri())
            {
                System.Diagnostics.Debug.WriteLine(item.AbsoluteUri);
                Assert.IsFalse(detector.ListProbabilityByUrls(item) >= Probability.Probably);
            }
        }
    }
}
