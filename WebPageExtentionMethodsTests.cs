﻿using HtmlAgilityPack;
using System.Collections.Generic;
using NUnit.Framework;
using CommonLibrary;

namespace ArticlesListDetector.Test
{
    [TestFixture]
    public class WebPageExtentionMethodsTests
    {
        //[Test]
        //public void H1Tags_HtmlWithH1Tags_H1TagsCount()
        //{
        //    // Arrange
        //    WebPage page = new WebPage(null);
        //    page.Html = @"<!doctype html>
        //                <html>
        //                <head> 
	       //                 <title></title>
        //                </head>
        //                <body>
        //                <div>
	       //                 <div>
		      //                  <h1> 1 </h1>
		      //                  <h1> 2 </h1>
	       //                 </div>
	       //                 <h1> 3 </h1>
        //                </div>
        //                </body>
        //                </html>";
            
        //    // Act
        //    int h1TagsCount = page.H1Tags().Count;

        //    // Assert
        //    Assert.IsTrue(h1TagsCount == 3);
        //}

        [Test]
        public void LinksToItself_HtmlWithLinksToItself_CorretLinkNumber()
        {
            // Arrange
            Link link = new Link("http://habrahabr.ru/hub/foto");

            WebPage page = new WebPage(link);
            page.InternalLinks = new List<Link>()
                {
                    new Link("http://habrahabr.ru/hub/foto"),
                    new Link("http://habrahabr.ru/hub/foto/"),
                    new Link("http://habrahabr.ru/hub/fotoaa"),
                    new Link("http://habrahabr.ru/hub/foasdasdto"),
                    new Link("http://habrahabr.ru/hub/")
                };
            // Act
            
            int count = page.LinksToItself().Count;

            // Assert

            Assert.IsTrue(count == 1);
        }

        [Test]
        public void ContinuationLink_HtmlWithPageLinks_ContinuationLinkCount()
        {
            var links = new List<Link>()
            {
                new Link("http://habrahabr.ru/interesting/page2/"),
                new Link("http://habrahabr.ru/interesting/lalala2/"),
                new Link("http://habrahabr.ru/interesting/page/2"),
                new Link("http://habrahabr.ru/interesting/2/"),
                new Link("http://habrahabr.ru/interesting?page=2"),
                new Link("http://habrahabr.ru/interesting2/"),
                new Link("http://habrahabr.ru/interesting/"),
            };

            Link link = new Link("http://habrahabr.ru/interesting/");

            WebPage page = new WebPage(link);
            page.InternalLinks = links;

            List<Link> contLinks = page.СontinuationLinks();

            Assert.IsTrue(contLinks.Count == 4);
        }

        //[TestMethod]
        //public void ChildPages_HtmlWithLinksStartsWithBase_LinksStartsWithBaseCount()
        //{
        //    WebPage page = new WebPage(new Link("http://habrahabr.ru/interesting/"));
        //    page.InternalLinks = new List<Link>()
        //    {
        //        new Link("http://habrahabr.ru/interesting/page2/"),
        //        new Link("http://habrahabr.ru/interesting/lalala/"),
        //        new Link("http://habrahabr.ru/interesting/page/2"),
        //        new Link("http://habrahabr.ru/interesting?asdasd=dasdad"),
        //        new Link("http://habrahabr.ru/interesting?page=2")
        //    };

        //    Assert.IsTrue(WebPageEstimateingMethods.ChildPages(page) == 3);
        //}


        //[TestMethod]
        //public void LastSegmentPageUriIsDigit_UriWithAndWithoutDigits_CorrectBoolResult()
        //{
        //    List<WebPage> pages = new List<WebPage>()
        //    {
        //        new WebPage(new Link("http://habrahabr.ru/interesting/page2/")), // 0
        //        new WebPage(new Link("http://habrahabr.ru/interesting/lalala/")), // 1
        //        new WebPage(new Link("http://habrahabr.ru/interesting/page/2")), // 2
        //        new WebPage(new Link("http://habrahabr.ru/interesting/page/2/")), // 3
        //        new WebPage(new Link("http://habrahabr.ru/interesting/2014-12-13")), // 4
        //        new WebPage(new Link("http://habrahabr.ru/interesting/1-12-12")), // 5
        //        new WebPage(new Link("http://habrahabr.ru/interesting/2014-12-13/")), // 6
        //        new WebPage(new Link("http://habrahabr.ru/interesting/20141213/")), // 7
        //        new WebPage(new Link("http://habrahabr.ru/interesting/20141213a")), // 8
        //    };


        //    Assert.IsFalse(WebPageEstimateingMethods.LastSegmentIsDigitOrDate(pages[0]));
        //    Assert.IsFalse(WebPageEstimateingMethods.LastSegmentIsDigitOrDate(pages[1]));
        //    Assert.IsTrue(WebPageEstimateingMethods.LastSegmentIsDigitOrDate(pages[2]));
        //    Assert.IsTrue(WebPageEstimateingMethods.LastSegmentIsDigitOrDate(pages[3]));
        //    Assert.IsTrue(WebPageEstimateingMethods.LastSegmentIsDigitOrDate(pages[4]));
        //    Assert.IsTrue(WebPageEstimateingMethods.LastSegmentIsDigitOrDate(pages[5]));
        //    Assert.IsTrue(WebPageEstimateingMethods.LastSegmentIsDigitOrDate(pages[6]));
        //    Assert.IsTrue(WebPageEstimateingMethods.LastSegmentIsDigitOrDate(pages[7]));
        //    Assert.IsFalse(WebPageEstimateingMethods.LastSegmentIsDigitOrDate(pages[8])); 
        //}

    }
}
